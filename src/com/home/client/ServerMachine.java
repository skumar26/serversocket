package com.home.client;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerMachine {
	
	//initialize socket and input stream 
    private Socket          clientSocket   = null; 
    private ServerSocket    server   = null; 
    private DataInputStream in       =  null; 
  
    // constructor with port 
    public ServerMachine(int port) 
    { 
        // starts server and waits for a connection 
        try
        { 
            server = new ServerSocket(port); 
            System.out.println("Server started"); 
  
            System.out.println("Waiting for a client ..."); 
    
        }
      catch(IOException i) 
      { 
          System.out.println(i); 
      } 
             
            
    } 
  
    public static void main(String args[]) throws IOException 
    { 
        ServerMachine machine = new ServerMachine(8082);
        machine.handleClientRequest(machine);
    }
    
    public ServerSocket getServerSocket() {
		return server;
	}
    
    private void handleClientRequest(ServerMachine machine) throws IOException {
    	ServerSocket serverSideSocket = machine.getServerSocket();
		while (true) {
			 clientSocket = serverSideSocket.accept();
			 System.err.println(clientSocket);
			 new Thread(() -> {
				 DataInputStream in = null;
				 DataOutputStream out = null;
				 Socket localSocket = null;
				try {
					localSocket = clientSocket;
					in = new DataInputStream(new BufferedInputStream(localSocket.getInputStream()));
					out = new DataOutputStream(new BufferedOutputStream(localSocket.getOutputStream()));
					System.err.println(in);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
					while (true) {
						
					   try {
						String value =  in.readUTF();
						System.err.println(Thread.currentThread());
						System.err.println(localSocket);
						out.writeUTF(value+"Hi");
						System.err.println(value);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					}
				}).start();
			 
			
		}

	}
} 


